package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserUnlockByLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "unlock user by login";

    @NotNull
    private final String NAME = "unlock-user-by-login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
