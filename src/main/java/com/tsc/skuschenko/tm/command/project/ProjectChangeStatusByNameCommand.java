package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "change project by name";

    @NotNull
    private final String NAME = "project-change-status-by-name";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("name");
        @NotNull final String value = TerminalUtil.nextLine();
        @NotNull final IProjectService projectService
                = serviceLocator.getProjectService();
        @Nullable Project project = projectService.findOneByName(userId, value);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project = projectService.changeStatusByName(
                userId, value, readProjectStatus()
        );
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
