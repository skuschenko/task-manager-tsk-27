package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "remove task by name";

    @NotNull
    private final String NAME = "task-remove-by-name";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("name");
        @NotNull final String value = TerminalUtil.nextLine();
        @NotNull final ITaskService taskService =
                serviceLocator.getTaskService();
        @Nullable final Task task = taskService.removeOneByName(userId, value);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
