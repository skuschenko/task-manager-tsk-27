package com.tsc.skuschenko.tm.command.data;

import com.tsc.skuschenko.tm.dto.Domain;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyBinaryPathException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Optional;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "load data from binary file";

    @NotNull
    private final String NAME = "data-load-bin";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileBinaryPath();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyBinaryPathException::new);
        @NotNull final FileInputStream fileInputStream =
                new FileInputStream(filePath);
        @NotNull final ObjectInputStream objectInputStream =
                new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
