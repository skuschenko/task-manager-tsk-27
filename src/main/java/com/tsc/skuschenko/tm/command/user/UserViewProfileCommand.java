package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "view profile of current user";

    @NotNull
    private final String NAME = "view-user-profile";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final User user =
                Optional.ofNullable(serviceLocator.getAuthService().getUser())
                        .orElseThrow(UserNotFoundException::new);
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
