package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProgramExitCommand extends AbstractCommand {

    @NotNull
    private final String DESCRIPTION = "exit";

    @NotNull
    private final String NAME = "exit";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
