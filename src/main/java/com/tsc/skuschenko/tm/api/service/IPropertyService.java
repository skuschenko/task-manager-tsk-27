package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.other.ISaltSetting;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthor();

    @NotNull
    String getAuthorEmail();

    @Nullable
    String getFileBase64Path();

    @Nullable
    String getFileBinaryPath();

}
